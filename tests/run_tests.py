#!/usr/bin/python3

import sys

from os import path

sys.path = [path.abspath(path.join(path.split(sys.argv[0])[0], '..'))] + \
            sys.path

import unittest

from test_fileinfo import FlacInfoTest, Mp3InfoTest

from test_musicdb import TestMusicDb

from test_gui import TestMainWindow

if __name__ == '__main__':
    unittest.main()
