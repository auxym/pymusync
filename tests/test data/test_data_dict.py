#!/usr/bin/python3

"""
Read test files and output a json file containing a list of dictionaries
with file information.

Used for creating known data for unit tests.
"""

import sys

import json

from os import path

from pymusync.musicdb import MusicDB

from tinydb.storages import MemoryStorage

from copy import copy

db = MusicDB.from_folder(sys.argv[1], storage=MemoryStorage)

jdic = {}

for f in db.all():
    j = copy(f)
    del j['_id']
    j['fname'] = path.relpath(j['fname'], sys.argv[1])

    jdic[f['_id']] = j

json.dump(jdic, sys.stdout, indent=4, sort_keys=True)
