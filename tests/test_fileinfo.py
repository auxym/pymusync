#!/usr/bin/python3

"""Test file info classes."""

import unittest

import os.path

from pymusync.musicfileinfo import FlacFileInfo, Mp3FileInfo

import json

import shutil

test_data_dir = 'test data/library'

with open('test data/known_data.json', 'rt') as f:
    known_data = json.load(f)


class FlacInfoTest(unittest.TestCase):
    tagattr = ['artist', 'album', 'date', 'albumartist', 'disc',
               'genre', 'title', 'tracknumber']
    fileattr = ['fname', 'fsize', 'mtime']
    infocls = FlacFileInfo
    ext = '.flac'

    def setUp(self):
        self.known_data = [v for v in known_data.values() if
                           os.path.splitext(v['fname'])[1].lower() == self.ext]

    def test_get_tags(self):
        for ref in self.known_data:
            fname = os.path.join(test_data_dir, ref['fname'])
            info = self.infocls(fname)
            info.scan()

            refkeys = sorted(list([k for k in ref if k in self.tagattr]))
            infokeys = sorted(list([k for k in info if k in self.tagattr]))

            self.assertEqual(refkeys, infokeys)

            for t in infokeys:
                self.assertEqual(ref[t], info[t])

    def test_attrdict(self):
        all_attr = self.tagattr + self.fileattr
        for ref in self.known_data:
            info = self.infocls()
            info.update(ref)
            ad = info.attrdict

            refkeys = sorted(list([k for k in ref if k in all_attr]))
            adkeys = sorted(list(ad.keys()))

            self.assertEqual(refkeys, adkeys)

            for t in refkeys:
                self.assertEqual(ref[t], ad[t])

    def test_write_tags(self):
        for ref in self.known_data:
            info = self.infocls()

            temp = os.path.join(test_data_dir, '..', 'temp', 'temp'+self.ext)
            shutil.copy(os.path.join(test_data_dir, '..', 'test data templates',
                                     'tone'+self.ext),
                        temp)

            info.update(ref)
            info['fname'] = temp
            info.save_tags()

            info = self.infocls(temp)
            info.scan()
            for tag in [t for t in ref if t in self.tagattr]:
                self.assertEqual(info[tag], ref[tag])

            os.remove(temp)

class Mp3InfoTest(FlacInfoTest):
    ext = '.mp3'
    infocls = Mp3FileInfo

if __name__ == "__main__":
    unittest.main()
