"""
musicdb.py
Copyright © 2014-2015 Francis Thérien

This file is part of pymusync.

Pymusync is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Pymusync is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Standard library
import os

import os.path

import json

from collections import defaultdict

# TinyDB
import tinydb

from tinydb import TinyDB, Table

from tinydb import where

# Our modules
from pymusync.musicfileinfo import MusicFileInfo

from pymusync.musicfileinfo import supported_extensions


class MusicDB(TinyDB):
    """Front-end to TinyDB database."""

    def __init__(self, *args, **kwargs):
        super(MusicDB, self).__init__(*args, **kwargs)
        self.root_dir = None

    def save(self, mfi):
        """Save MusicFileInfo object to db.

        Checks if the file is already in the db.

        """
        self.remove(where('fname') == mfi['fname'])
        self.insert(mfi)

    def table(self, name='_default'):
        """
        Get access to a specific table.

        Creates a new table, if it hasn't been created before, otherwise it
        returns the cached :class:`MusicTable` object.

        :param name: The name of the table.
        :type name: str
        """

        if name in self._table_cache:
            return self._table_cache[name]

        table = MusicTable(name, self)
        self._table_cache[name] = table
        return table

    def _get_distinct(self, field, *altfields):
        """Return list of unique values for a given field."""
        selector = where(field)
        for af in altfields:
            selector = selector | where(af)
        vals = self.search(selector)

        def found_field(val):
            try:
                return val[field]
            except KeyError:
                for f in altfields:
                    if f in val:
                        return val[f]
            # We shoudn't ever get here
            assert False

        vals = [found_field(v) for v in vals]

        # Make values distinct
        vals = list(set(vals))

        return vals

    def walk_dir(self, purge=True):
        """Walk a directory tree and update DB with found music files.

        If purge is True (default), then remove db entries for which the
        file does not exist anymore.

        """

        dirname = self.root_dir

        if not os.path.exists(dirname):
            raise ValueError("Invalid path %s" % dirname)

        exist_fnames = []

        for (dirpath, dirnames, filenames) in os.walk(dirname):
            for f in [h for h in filenames if os.path.splitext(h)[1].lower()
                      in supported_extensions]:

                fullname = os.path.join(dirpath, f)

                existing = self.search(where('fname') == fullname)
                assert len(existing) <= 1
                if existing:
                    existing = MusicFileInfo(**existing[0])

                changed = False if not existing else existing.has_changed()

                if (not existing) or changed:
                    mfi = MusicFileInfo(fullname)
                    mfi.scan()
                    self.save(mfi.attrdict)

                exist_fnames.append(fullname)

        if purge:
            del_names = [item['fname'] for item in self.all() if
                         item['fname'] not in exist_fnames]
            for name in del_names:
                self.remove(where('fname') == name)

    @classmethod
    def from_folder(cls, folder, *args, **kwargs):
        db = cls(*args, **kwargs)
        db.root_dir = os.path.abspath(folder)
        db.walk_dir()
        return db


class MusicTable(Table):
    def has_song(self, sdict):
        sel = where('fname')
        tags = ('artist', 'album', 'tracknumber', 'date', 'title')
        for tag in tags:
            if tag in sdict:
                sel = sel & (where(tag) == sdict[tag])

        return self.get(sel)

    def make_copy_with_default(self, dest=None, **kwargs):
        """Return in-memory copy of db with default values inserted."""
        if dest is not None:
            newdb = dest
        else:
            newdb = MusicDB(storage=tinydb.storages.MemoryStorage)

        def has_key(k, mfi):
            if k not in mfi:
                return False
            return bool(mfi[k])

        for it in self.all():
            for k in kwargs:
                if not has_key(k, it):
                    it[k] = kwargs[k](it)
            newdb.insert(it)
        return newdb

    def build_tree(self, fields, replace):
        """Classify db data into an arbitrary tree structure."""

        def mktree():
            """https://gist.github.com/hrldcpr/2012250"""
            return defaultdict(mktree)

        def placeitem(s_dict, keys, item):
            """Recursively access a list of keys in a dictionary.

            This works with our default-dict tree because dictionary entries
            are created on-the-fly on access.

            """

            if keys:
                placeitem(s_dict[keys.pop()], keys, item)
            else:
                _id = item['_id']
                s_dict[_id] = item
                return

        def emptystring(it):
            return ''

        for f in fields:
            if f not in replace.keys():
                replace[f] = emptystring

        rfields = list(reversed(fields))

        tree = mktree()

        for item in self.all():

            # Insert replacement values if item is missing fields
            for rfield, rfunc in replace.items():
                has_key = bool(item[rfield]) if rfield in item else False
                if not has_key:
                    item[rfield] = rfunc(item)

            placeitem(tree, [item[k] for k in rfields], item)

        return tree

    def pprint(self):
        return json.dumps(self.all(), sort_keys=True, indent=4)
