#!/usr/bin/python3

"""Test file info classes."""

import unittest

import os.path

import tinydb

from tinydb import where

from pymusync.musicdb import MusicDB

import json

test_data_dir = 'test data/library'


test_data_dir = os.path.join(os.path.split(__file__)[0], 'test data')


class TestMusicDb(unittest.TestCase):

    def setUp(self):
        with open('test data/known_data.json', 'rt') as f:
            known_data = json.load(f)
        self.known_data = [v for v in known_data.values()]

        with open('test data/known_tree.json', 'rt') as f:
            self.known_tree = json.load(f)

    def test_insert_new(self):
        db = MusicDB(storage=tinydb.storages.MemoryStorage)
        for d in self.known_data:
            db.save(d)

        self.assertEqual(len(self.known_data), len(db.all()))

        for d in self.known_data:
            cur = db.search(where('fname') == d['fname'])
            self.assertEqual(len(cur), 1)
            self.assertEqual(d, cur[0])

    def test_build_tree(self):
        db = MusicDB(storage=tinydb.storages.MemoryStorage)
        for d in self.known_data:
            db.save(d)

        tree = db.build_tree("artist", "album")

        self.assertEqual(tree, self.known_tree)

if __name__ == "__main__":
    unittest.main()
