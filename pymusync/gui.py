"""
gui.py
Copyright © 2014-2015 Francis Thérien

This file is part of pymusync.

Pymusync is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Pymusync is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt4 import QtCore, QtGui

import os

import os.path

import itertools

import hashlib

import base64

import warnings

from tinydb.storages import JSONStorage

from tinydb.middlewares import CachingMiddleware

from pymusync.musicdb import MusicDB

from pymusync.musicfileinfo import is_lossy

from pymusync import encode


class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setStorageLocations()

        self.setWindowTitle('pymusync')

        self.resize(450, 600)

        # Create the menu.
        test_menu = self.menuBar().addMenu('File')
        test_menu.addAction('Exit')

        # Create directory selection combo boxes/labels/toolbuttons.
        self.master_cmb = DirectoryWidget(labelstr='Source:')
        self.slave_cmb = DirectoryWidget(labelstr='Destination:')

        # Create the tool buttons.
        scan_btn = QtGui.QPushButton('Scan')
        run_btn = QtGui.QPushButton('Run')
        btn_hlayout = QtGui.QHBoxLayout()
        btn_hlayout.addWidget(scan_btn)
        btn_hlayout.addWidget(run_btn)

        scan_btn.clicked.connect(self.scan)
        run_btn.clicked.connect(self.run)

        # Create the album tree.
        self.album_tree = AlbumTreeWidget()

        # Layout stuff vertically.
        vlayout = QtGui.QVBoxLayout()
        vlayout.addWidget(self.master_cmb)
        vlayout.addWidget(self.slave_cmb)
        vlayout.addWidget(self.album_tree)
        vlayout.addLayout(btn_hlayout)

        widget = QtGui.QWidget(self)
        widget.setLayout(vlayout)
        self.setCentralWidget(widget)

        self.master_cmb.combo.setEditText(
            "/home/francis/PythonProjects/pymusync/tests/test data/library/")
        self.slave_cmb.combo.setEditText(
            "/home/francis/PythonProjects/pymusync/tests/test data/remote/")

    def setStorageLocations(self):
        """Get storage dir according to freedesktop standard."""
        ds = QtGui.QDesktopServices()
        self.data_dir = ds.storageLocation(ds.DataLocation)
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def dbFromFolder(self, dir):
        dir = os.path.abspath(dir)

        dbfile = os.path.join(self.data_dir,
                              base64.urlsafe_b64encode(
                                  hashlib.md5(dir.encode('utf8')).digest()
                              ).decode('utf-8') + '.json')

        db = MusicDB.from_folder(dir, dbfile,
                                 storage=CachingMiddleware(JSONStorage))
        return db

    def scan(self):
        """Scan selected directories to populate album list."""
        mst_db, slv_db = map(self.dbFromFolder, (
            self.master_cmb.currentText(), self.slave_cmb.currentText()
            )
        )

        self.album_tree.populate(mst_db, slv_db)

        self.mst_db = mst_db
        self.slv_db = slv_db

    def run(self):
        jobs = []
        input_root = os.path.abspath(self.master_cmb.currentText())
        output_root = os.path.abspath(self.slave_cmb.currentText())

        for album_item in self.album_tree.allAlbums():
            artist = album_item.parent().text(0)
            album_name = album_item.text(0)
            date = album_item.text(1)

            songs = list(self.album_tree.mst_tree
                         [artist][date][album_name].values())

            is_in_remote = self.slv_db.has_song(songs[0])
            want_in_remote = album_item.checkState(0) == QtCore.Qt.Checked

            if want_in_remote and not is_in_remote:
                # Queue an encode or copy job
                for mfi in songs:
                    input_name = mfi['fname']
                    output_name = input_name.replace(input_root, output_root)

                    # Copy files if already lossy, else encode in mp3
                    if is_lossy(input_name):
                        job_cls = encode.CopyJob
                    else:
                        job_cls = encode.EncodeJob
                        output_name = os.path.splitext(output_name)[0] + '.mp3'

                    jobs.append(job_cls(input_name, output_name))

            elif not want_in_remote and is_in_remote:
                # Queue a delete job
                for mfi in songs:
                    slv_mfi = self.slv_db.has_song(mfi)
                    if slv_mfi:
                        jobs.append(encode.RemoveJob(slv_mfi['fname']))
                    else:
                        warnings.warn("Could not find file %s" % mfi['fname'])

        progDlg = EncodeProgressWindow(parent=self, n_threads=4)
        progDlg.setRange(0, len(jobs))

        encoder = encode.MultiThreadedEncoder(jobs, n_threads=4,
                                              dialog=progDlg)

        t = QtCore.QThread()
        encoder.moveToThread(t)
        t.started.connect(encoder.run)
        t.start()

        progDlg.exec_()

        self.scan()

        SuccessMessageBox().exec_()


class SuccessMessageBox(QtGui.QMessageBox):
    def __init__(self):
        super(SuccessMessageBox, self).__init__()
        self.setText("Sync done!")


class AlbumTreeWidget(QtGui.QTreeWidget):
    def __init__(self, parent=None):
        super(AlbumTreeWidget, self).__init__(parent)

        self.setColumnCount(3)
        self.setHeaderLabels(('Album List', 'Date', 'Codec'))
        self.header().resizeSection(0, 300)
        self.header().resizeSection(1, 70)
        self.header().resizeSection(2, 70)

        self.itemChanged.connect(self.handle_check_item)

    def populate(self, mst_db, slv_db):

        def dft_albumartist(it):
            try:
                return it['artist']
            except KeyError:
                return ''

        def dft_date(it):
            return ''

        self.clear()

        self.mst_tree = mst_db.build_tree(['albumartist', 'date', 'album'],
                                          {'albumartist': dft_albumartist,
                                          'date': dft_date}
                                          )

        for artist, dates in sorted(self.mst_tree.items()):
            it = QtGui.QTreeWidgetItem(self)
            it.setText(0, artist)
            it.setFlags(it.flags() or QtCore.Qt.ItemIsUserCheckable
                        or QtCore.Qt.ItemIsTristate)
            it.setCheckState(0, QtCore.Qt.Unchecked)

            # Form tuples of (album, date, songs) from tree
            allalbums = []
            for date, albums in dates.items():
                salbums = list(sorted(albums.keys()))

                songs = [list(albums[aname].values()) for aname in salbums]

                albums_tup = [(a, d, s) for a, d, s in
                              zip(salbums, [date]*len(salbums), songs)]

                allalbums += albums_tup

            for album, date, songs in sorted(allalbums):
                it_alb = QtGui.QTreeWidgetItem(it)
                it_alb.setText(0, album)
                it_alb.setText(1, date)

                it_alb.setFlags(it_alb.flags() or
                                QtCore.Qt.ItemIsUserCheckable)

                it_alb.setCheckState(0, QtCore.Qt.Unchecked)

                codec = os.path.splitext(songs[0]['fname'])[1][1:].lower()
                it_alb.setText(2, codec)

                # Check if the album is in the slave db
                if slv_db.has_song(songs[0]):
                    it_alb.setCheckState(0, QtCore.Qt.Checked)

    def handle_check_item(self, itm):
        """Implement traditional checkbox logic.

        * If parent is checked, all children get checked, and conversely.
        * If some but not all children are checked, parent is partially
          checked.

        """

        parent = itm.parent()
        state = itm.checkState(0)
        self.blockSignals(True)

        if parent is None:
            for i in range(itm.childCount()):
                itm.child(i).setCheckState(0, state)
        else:
            checkstates = [(parent.child(i).checkState(0) == QtCore.Qt.Checked)
                           for i in range(parent.childCount())]
            all_children_checked = all(checkstates)
            all_children_unchecked = not any(checkstates)

            if all_children_checked:
                parent.setCheckState(0, QtCore.Qt.Checked)
            elif all_children_unchecked:
                parent.setCheckState(0, QtCore.Qt.Unchecked)
            else:
                parent.setCheckState(0, QtCore.Qt.PartiallyChecked)

        self.blockSignals(False)

    def allAlbums(self):
        """Return iterator over all album (sub)items."""
        def take_toplevelitem(i):
            return self.topLevelItem(i)

        def take_childitems(parent):
            indices = range(parent.childCount())

            def take_child(i):
                return parent.child(i)

            return map(take_child, indices)

        artist_indices = self.topLevelItemCount()
        artist_items = map(take_toplevelitem, range(artist_indices))

        album_items = itertools.chain.from_iterable(map(take_childitems,
                                                    artist_items))

        return album_items


class DirectoryWidget(QtGui.QWidget):
    def __init__(self, parent=None, dbdir=None, labelstr=''):
        super(DirectoryWidget, self).__init__(parent)

        button = QtGui.QToolButton()
        self.combo = QtGui.QComboBox()
        label = QtGui.QLabel(labelstr)

        icon = QtGui.QIcon.fromTheme('folder-open')
        button.setIcon(icon)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(label, 0)
        layout.addWidget(self.combo, 1)
        layout.addWidget(button, 0)

        self.setLayout(layout)

        button.clicked.connect(self.browseFolder)
        self.combo.setEditable(True)

    def browseFolder(self):
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(2)
        dialog.setOption(dialog.ShowDirsOnly, True)

        if dialog.exec():
            folderName = dialog.selectedFiles()[0]
            allItems = [self.combo.itemText(i)
                        for i in range(self.combo.count())]

            if folderName not in allItems:
                self.combo.addItem(folderName)
                allItems = [self.combo.itemText(i)
                            for i in range(self.combo.count())]

            self.combo.setCurrentIndex(allItems.index(folderName))

    def currentText(self):
        return self.combo.currentText()


class EncodeProgressWindow(QtGui.QProgressDialog):
    def __init__(self, parent, n_threads=4):
        super(EncodeProgressWindow, self).__init__(parent)

        self.label_lines = []
        for i in range(n_threads):
            self.label_lines.append("%d: %s" % (i, 'None'))
            self.setLabelText('\n'.join(self.label_lines))

    def editLabelLine(self, index, p_str):
        self.label_lines[index] = "%d: %s" % (index, p_str)
        self.setLabelText("\n".join(self.label_lines))

    def handleJobDone(self, index, remain):
        self.editLabelLine(index, "None")
        self.setValue(self.maximum() - remain)

    def handleJobStart(self, index, p_str):
        self.editLabelLine(index, p_str)


def main():
    app = None
    if not app:
        app = QtGui.QApplication([])
    app.setOrganizationName('pymusync')

    window = MainWindow()
    window.show()

    if app:
        app.exec_()

if __name__ == '__main__':
    main()
