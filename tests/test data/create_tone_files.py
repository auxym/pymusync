#!/usr/bin/python3

"""
Create test data from a tree of real music files.

Usage create_tone_files input_tree/

For each given in the input tree, copy a template file of the same format
that consists of 1 s of a 440 Hz sine. Then all tags are copied from the
original file.
"""

import mutagenx.flac

import mutagenx.oggvorbis

import mutagenx.mp3

import mutagenx.mp4

import os

import sys

from os import path

import shutil


class TagCopier:
    tag_cls = {'.flac': mutagenx.flac.FLAC,
               '.mp3': mutagenx.mp3.MP3,
               '.m4a': mutagenx.mp4.MP4,
               '.ogg': mutagenx.oggvorbis.OggVorbis,
               }

    def __init__(self, src, dst):
        self.fmt = path.splitext(src)[1].lower()
        self.src, self.dst = self.read_tags(src), self.read_tags(dst)

    def _copy_id3(self):
        for frame in self.src.tags:
            self.dst.tags.add(self.src.tags[frame])

    def _copy_tags(self):
        for t in self.src.tags:
            self.dst.tags[t] = self.src.tags[t]

    def copy(self):
        self.dst.delete()
        try:
            self.dst.add_tags()
        except:
            pass

        if self.fmt == '.mp3':
            self._copy_id3()
        else:
            self._copy_tags()

        self.dst.save()

    def read_tags(self, f):
        cls_ = self.tag_cls[self.fmt]
        obj = cls_(f)
        return obj


def main():
    src = sys.argv[1]
    dst = '.'
    template_src = '../test data templates'

    for (dirpath, dirnames, filenames) in os.walk(src):
        for f in [h for h in filenames if path.splitext(h)[1].lower()
                  in TagCopier.tag_cls]:
            print(f)
            ext = path.splitext(f)[1]
            relpath = path.relpath(dirpath, src)
            cpath = path.join(dst, relpath)

            if not path.exists(cpath):
                os.makedirs(cpath)

            src_file = path.join(template_src, 'tone' + ext.lower())
            dst_file = path.join(cpath, f)

            shutil.copy(src_file, dst_file)
            TagCopier(path.join(dirpath, f), dst_file).copy()

if __name__ == '__main__':
    main()
