"""
musicfileinfo.py
Copyright © 2014-2015 Francis Thérien

This file is part of pymusync.

Pymusync is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Pymusync is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Standard library
import os

# mutagenx
import mutagenx.flac

import mutagenx.mp3

import mutagenx.mp4

import mutagenx.oggvorbis


class MusicFileInfoBase(dict):
    """Base class for music file metadata, should be subclassed."""

    tagattr = ['artist', 'album', 'date', 'albumartist', 'disc',
               'genre', 'title', 'tracknumber']
    fileattr = ['fname', 'fsize', 'mtime']

    def __init__(self, fname=None, **kwargs):
        super(MusicFileInfoBase, self).__init__(**kwargs)
        if fname is not None:
            self['fname'] = os.path.abspath(fname)

    def scan(self):
        self.get_basic_info()
        self.get_tags()

    def get_basic_info(self, update=True):
        """Get basic file info such as date and size."""
        stat = os.stat(self['fname'])
        if update:
            self['fsize'] = stat.st_size
            self['mtime'] = stat.st_mtime
        return dict(fsize=stat.st_size,
                    mtime=stat.st_mtime)

    def has_changed(self):
        stat = self.get_basic_info(update=False)
        return stat['mtime'] == self['mtime'] and \
            stat['fsize'] == self['fsize']

    @property
    def is_lossy(self):
        return is_lossy(self['fname'])

    @property
    def is_lossless(self):
        return not self.is_lossy

    @property
    def format(self):
        """Music file format"""
        return os.path.splitext(self['fname'])[1].upper()[1:]

    @property
    def attrdict(self):
        """Return dict of all attributes for storing in database."""
        allkeys = self.fileattr + self.tagattr
        return dict(**{k: v for k, v in self.items() if k in allkeys})

    @staticmethod
    def _mutagenx_getter(tobj, tname):
        """Use mutagenx to read a given tag from file.

        This generic function covers most cases. Needs to be overrided
        eg. for ID3 tags.

        Arguments:
        **tobj**
            mutagenx tag object

        **tname**
            Frame name for tag

        """

        return tobj[tname][0]

    @staticmethod
    def _mutagenx_putter(tobj, tname, val):
        tobj[tname] = [val]

    def _mutagen_deltag(self, tobj, tname):
        del tobj[tname]

    def _get_tag_obj(self):
        mobj = self._mutagenx_cls(self['fname'])

        try:
            tags = mobj.tags
        except:
            mobj.add_tags()

        if mobj.tags is None:
            mobj.add_tags()

        return mobj
    def get_tags(self):
        try:
            tags = self._get_tag_obj().tags
        except:
            return
        for k in self.tagattr:
            frame = self.tags[k]
            try:
                val = self._mutagenx_getter(tags, frame)
                val = self._tag_read_post_process(k, val)
            except (IndexError, KeyError):
                val = ''
            else:
                self[k] = val

    def save_tags(self):
        fdata = self._get_tag_obj()
        tags = fdata.tags
        for k in self.tagattr:
            try:
                val = self[k]
            except KeyError:
                val = None

            frame = self.tags[k]
            if val is not None:
                val = self._tag_write_pre_process(k, val)
                self._mutagenx_putter(tags, frame, val)
            elif k in tags:
                self._mutagen_deltag(tags, frame)

        fdata.save()

    @staticmethod
    def _tag_write_pre_process(key, val):
        """Do nothing in this base class.

        Subclasses can override this to modify how tags are written to the
        mutagen tag object.

        """

        return str(val)

    @staticmethod
    def _tag_read_post_process(key, val):
        """Do nothing in this base class.

        Subclasses can override this to modify how tags are read from the
        mutagen tag object.

        """

        return val

    def print_(self):
        for k in self.tagattr:
            print("%s : %s" % (k, self[k]))

    def copy_tags_from(self, fname):
        """Copy tags from another file"""
        other = MusicFileInfo(fname)
        other.scan()
        for k in self.tagattr:
            try:
                self[k] = other[k]
            except KeyError:
                try:
                    del self[k]
                except KeyError:
                    pass
        self.save_tags()


class Mp3FileInfo(MusicFileInfoBase):
    tags = {'artist': 'TPE1',
            'album': 'TALB',
            'date': 'TDRC',
            'albumartist': 'TPE2',
            'disc': 'TPOS',
            'genre': 'TCON',
            'title': 'TIT2',
            'tracknumber': 'TRCK'}

    _mutagenx_cls = mutagenx.mp3.MP3

    @staticmethod
    def _mutagenx_getter(tobj, tname):
        return str(tobj.getall(tname)[0])

    @staticmethod
    def _mutagenx_putter(tobj, tname, val):
        id3tag = getattr(mutagenx.id3, tname)(encoding=3, text=[val])
        tobj.setall(tname, [id3tag])

    def _mutagen_deltag(self, tobj, tname):
        tobj.delall(tname)

class FlacFileInfo(MusicFileInfoBase):
    tags = {'artist': 'artist',
            'album': 'album',
            'date': 'date',
            'albumartist': 'album artist',
            'disc': 'disc',
            'genre': 'genre',
            'title': 'title',
            'tracknumber': 'tracknumber'}

    _mutagenx_cls = mutagenx.flac.FLAC


class M4aFileInfo(MusicFileInfoBase):
    _mutagenx_cls = mutagenx.mp4.MP4

    tags = {'artist': b'\xa9ART',
            'album': b'\xa9alb',
            'date': b'\xa9day',
            'albumartist': b'aART',
            'disc': b'disk',
            'genre': b'\xa9gen',
            'title': b'\xa9nam',
            'tracknumber': b'trkn'}


    @staticmethod
    def _tag_read_post_process(key, val):
        if key in ('disc', 'tracknumber') and val:
            val = str(val[0])
        return val

    @staticmethod
    def _tag_write_pre_process(key, val):
        if key in ('disc', 'tracknumber'):
            val = (int(val), )
        return val


class OggFileInfo(MusicFileInfoBase):
    tags = {'artist': 'artist',
            'album': 'album',
            'date': 'date',
            'albumartist': 'album artist',
            'disc': 'disc',
            'genre': 'genre',
            'title': 'title',
            'tracknumber': 'tracknumber'}

    _mutagenx_cls = mutagenx.oggvorbis.OggVorbis

_lossy_extensions = ['.mp3', '.m4a', '.ogg']
_lossless_extensions = ['.flac']
supported_extensions = _lossy_extensions + _lossless_extensions


def is_lossy(fname):
    ext = os.path.splitext(fname)[1].lower()
    if ext not in supported_extensions:
        raise ValueError("Unsupported extension %s" % ext)
    return ext in _lossy_extensions

dispatch_dict = {'.mp3': Mp3FileInfo,
                 '.m4a': M4aFileInfo,
                 '.ogg': OggFileInfo,
                 '.flac': FlacFileInfo}


def MusicFileInfo(fname=None, **kwargs):
    kname = kwargs['fname'] if not fname else fname
    ext = os.path.splitext(kname)[1].lower()
    return dispatch_dict[ext](fname, **kwargs)
