"""
encode.py
Copyright © 2014-2015 Francis Thérien

This file is part of pymusync.

Pymusync is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Pymusync is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# http://www.ibm.com/developerworks/aix/library/au-threadingpython/

# http://stackoverflow.com/questions/6783194/background-thread-with-qthread-in-pyqt

from PyQt4 import QtCore

import string

import subprocess

import os

import os.path

import shutil

import queue

import shlex

from pymusync.musicfileinfo import MusicFileInfo


class Job():
    def __init__(self, input, output=None):
        self.input = input
        self.output = output

    def run(self):
        pass


class EncodeJob(Job):
    decode_cmd = {'flac': 'flac -d -o "$output" "$input"'}
    encode_cmd = {'lame_v4': 'lame -h -V4 "$input" "$output"'}
    status_string = "Encoding"

    def __init__(self, input, output, codec='lame_v4'):
        super(EncodeJob, self).__init__(input, output)
        self.codec = codec

    @staticmethod
    def make_sp_args(cmd, input, output):
        cmd = string.Template(cmd)
        cmd = cmd.substitute(input=input, output=output)
        return shlex.split(cmd)

    def run(self):
        input_fmt = os.path.splitext(self.input)[1][1:].lower()
        wavname = os.path.splitext(self.output)[0] + '.wav'

        if not os.path.exists(os.path.dirname(self.output)):
            os.makedirs(os.path.dirname(self.output))

        decoder_cmd = self.decode_cmd[input_fmt]
        decoder_input = self.input
        decoder_output = wavname

        encoder_cmd = self.encode_cmd[self.codec]
        encoder_input = wavname
        encoder_output = self.output

        subprocess.call(self.make_sp_args(decoder_cmd, decoder_input,
                                          decoder_output), stdout=None)

        subprocess.call(self.make_sp_args(encoder_cmd, encoder_input,
                        encoder_output), stdout=None)

        # TODO should check for success
        os.remove(wavname)

        # Copy tags
        mfi_output = MusicFileInfo(self.output)
        mfi_output.copy_tags_from(self.input)


class CopyJob(Job):
    status_string = "Copying"

    def __init__(self, input, output):
        super(CopyJob, self).__init__(input, output)

    def run(self):
        shutil.copy(self.input, self.output)


class RemoveJob(Job):
    status_string = "Removing"

    def run(self):
        try:
            os.remove(self.input)
        except IOError as e:
            print(e)


class Encoder(QtCore.QObject):
    jobDone = QtCore.pyqtSignal(int, int)
    jobStart = QtCore.pyqtSignal(int, str)

    def __init__(self, queue, threadId):
        super(Encoder, self).__init__()
        self.queue = queue

        self.threadId = threadId

    @QtCore.pyqtSlot()
    def run(self):
        while True:
            job = self.queue.get()

            self.jobStart.emit(self.threadId,
                               ' '.join([job.status_string,
                                         os.path.split(job.input)[-1]
                                         ])
                               )
            job.run()

            self.jobDone.emit(self.threadId, self.queue.qsize())
            self.queue.task_done()


class MultiThreadedEncoder(QtCore.QObject):
    allJobsDone = QtCore.pyqtSignal()

    def __init__(self, jobs, n_threads=4, dialog=None):
        super(MultiThreadedEncoder, self).__init__()
        self.queue = queue.Queue()
        self.jobs = jobs
        self.n_threads = n_threads
        self.dialog = dialog

        self.thread_pool = []
        self.worker_pool = []

    @QtCore.pyqtSlot()
    def run(self):
        # Do some preparation
        self.makeDirStructure()

        # Populate queue with data and spawn the threads
        for job in self.jobs:
            self.queue.put(job)

        self.spawn_thread_pool()

        # wait on the queue until everything has been processed
        self.queue.join()
        self.allJobsDone.emit()

        while self.thread_pool:
            t = self.thread_pool.pop()
            t.exit()
            t.wait()

        self.dialog.setValue(self.dialog.maximum())

    def spawn_thread_pool(self):
        """Spawn a pool of threads and pass them queue instance."""
        for i in range(self.n_threads):

            worker = Encoder(self.queue, i)
            if self.dialog is not None:
                worker.jobDone.connect(self.dialog.handleJobDone)
                worker.jobStart.connect(self.dialog.handleJobStart)

            t = QtCore.QThread()
            worker.moveToThread(t)
            t.started.connect(worker.run)

            # Keep references to thread and worker instances so
            # that they are not garbage collected
            self.worker_pool.append(worker)
            self.thread_pool.append(t)

            t.start()

    def makeDirStructure(self):
        """Make sure all the output dirs exist."""
        for job in self.jobs:
            try:
                output_dir = os.path.dirname(job.output)
                output_exists = os.path.exists(output_dir)
            except AttributeError:
                output_dir = None
                output_exists = True

            if (not output_exists) and (output_dir is not None):
                os.makedirs(output_dir)
