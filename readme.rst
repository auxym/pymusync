=======================================
    PyMuSync
=======================================

A small application to manage selective synchronization of a "main"
music library with a "mobile" library, e.g. an mp3 player.

PyMuSync is currently in an early *barely functional* state and
still has a lot of rough edges, but feel free to try it out and suggest
improvements.

Use case and features
=======================================
Pymusync was written to scratch one specific itch. I wanted a tool
that would:

+ Scan my music library, reading the tags to populate a list of albums.

+ Let me graphically select albums I want to have on my
  mobile device.
+ Populate/synchronize my mobile library according to my choices, using the
  following logic:

  - If I want to copy an album that is in lossless format (e.g. FLAC),
    then it should be encoded to a lossy format (eg. MP3).
  - If the album is already in a lossy format, then copy files as-is,
    do *not* transcode.
  - When transcoding, tags should be automatically copied from the
    source file to the encoded file.

Pymusync currently supports the following formats:

+ FLAC
+ MP3
+ AAC (.m4a)
+ Vorbis (.ogg)

Installation
=======================================
Once the following dependencies are met, simply clone the repo and run ``run.py``

Dependencies
---------------------------------------
Pymusync is written in python and Qt and should run on most OS. It has however 
only been tested on linux.

Python:
~~~~~~~~
+ Python (>= 3.3)
+ TinyDB (>= 1.1.0, available on PyPI, ``pip install tinydb``)
+ PyQt4 (available on PyPI, ``pip install PyQt4``)
+ mutagenx (available on PyPI, ``pip install mutagenx``)

Codecs:
~~~~~~~~
The following binaries need to be in your path:

+ ``flac``
+ ``lame``

``flac`` and ``lame`` packages should be available from your distribution's
repositories. Windows users can get binaries from `<www.rarewares.org>`_.

Usage
=======================================
1. Select the `source` and `destination` locations.
2. Click `scan` to scan both directory trees and populate the album list
3. Check the albums you want to have in your destination library,
   uncheck the ones you want removed.

   **Warning: Pymusync will permanently delete files from the destination
   directory. Make sure you have other copies of these files.**

4. Click `run` to do the work!

Status
=======================================
Pymusync is currently at version 0.1.0.

Missing features
-----------------

+ An easy_install script.
+ Only transcoding from flac to mp3 is currently possible. Furthermore,
  mp3 encoding is not configurable (lame -V4 is hardcoded)
+ Pymusync works at album level and thus does not allow selecting individual
  tracks

Known Issues
-----------------
+ The tests really suck

License
=======================================
Pymusync uses the mutagenx library to deal with tags (it's great), which is
distributed under the GPL (v2). To comply, I have chosen to distribute
pymusync under the GPL (v3).
