#!/usr/bin/python

import unittest

import sys

from PyQt4 import QtGui

from pymusync import gui


class TestMainWindow(unittest.TestCase):

    def setUp(self):
        self.app = QtGui.QApplication(sys.argv)
        self.window = gui.MainWindow()

    def testScan(self):
        self.window.scan()
